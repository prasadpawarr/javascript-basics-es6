/* Comments 
 // - inline comment 
 /* - multiline comment 
*/

/* Data types 
    - string
    - boolean = true or false
    - number = a numerical value
    - undefined = variable not set to anything
    - null = set to nothing
    - symbol
    - object = stores multiple key-value pairs
*/

/* Variable declaration
- three methods
    var myName = "Prasad"
    
    let myName = "Poonam"
    
    const pi = 3.14
*/

/* strings are immutable individually 
    that is individual letters cannot be changed
    
    eg: var string = "Jello world";
        string[0] = "H";
        throws an error
        
*/
//____________________________________________________________________________
/* Array functions - 
adds element to the end   --> array.push(10);
adds element to the start --> array.unshift(11);
removes lastElement       --> array.pop();
removes firstElement      --> array.shift();
*/

//functions in javacript:
// function firstFunction(){
//     console.log("Im in a function lol WASUP Bisch")
// }
// firstFunction()
//______________________________________________________________________
// function queue(arr, item){
//     arr.push(item)
//     arr.shift()    
// }

// var newList = [1, 2, 3, 4, 5]

// console.log("Before : [" + (newList) + "]") 
// //or console.log("Before: "+JSON.stringify(newList))
// queue(newList, 10);
// console.log("After : [" + (newList) + "]")
//_____________________________________________________________________
// function strictTest(val){
//     if(val === '10')
//         return "val is equal numerically and same datatype"
//     else
//         if(val == 10)
//             return "numerically only same"
//         else
//             return "val is different totally"
// }

// console.log(strictTest('10'))

/* Equality operator:
    == does a type conversion and then checks
        returns true for 10 == '10'
        
    === strictly checks for data type and value
        returns false for 10 === '10'
*/

/* Switch case  */


// function switchCase(val){
//     var ans = "";

//     switch(val){
//     case 1: 
//         console.log("alpha");
//         break;
//     case 2: 
//         console.log("beta")
//         break
//     case 3: 
//         console.log("gamma")
//         break
//     case 4: 
//         console.log("delta")  
//         break
//     default:
//         console.log("LMAO fail")    
//     }
//     return ans;    
// }
// switchCase(221)


// Generate a random number btw [0, 1)

// var a = Math.random()
// console.log(a)

// // to get a random whole number

// var c = Math.floor(a * 20)
// console.log(c)


//PARSE INT FUNCTION - converts string to int

// var str = "102"
// console.log(parseInt(str))

/* ternary operator

    condition ? statement-if-true : statement-if-false
    
*/
// var a = 12, b = 11;
// var eq = (a == b)? true : false
// console.log(eq)

// var num = 0
// var sign = num > 0 ? "positive" : (num < 0 ? "negative" : "zero")
// console.log(sign)