// Objects in java script: 
// objs are key-value pairs and can be used as a dictionary

var myDog = {
    "name": "Shadow",
    "age": 3,
    "breed": "German Shepherd",
    "friends": ["tokyo", "mexico", "helsinki"]
};

var name = myDog["name"];
var age = myDog.age;
var friends = myDog.friends;
console.log(name)
    //------------------------------------------------------------
    // to add a property in the object
    // simply use dot notation and assign the required value like
myDog.speak = "Bark"
console.log(myDog["speak"])
    //-------------------------------------------------------------
    // to delete a property use "delete" keyword:
delete myDog.age
    //-------------------------------------------------------------
    // to check if an obj has a particular property or no - gives T/F
var result = myDog.hasOwnProperty("friends")
console.log(result)