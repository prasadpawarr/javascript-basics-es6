# Javascript-Tutorial.

*A tutorial/notes for javascript.*




### Basics => variables/datatypes/arrays(functions)/operators/switch/conditionals

### Scope-rules => global/local-scope-rules

### Record-collection => declaration-of-nested-records/accessing/functions.

### Objects => declaration/object-functions

### ES6 => var/let/const/Anonymous-arrow-functions/template-literal(`)