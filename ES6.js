/*  
    1. var and let keywords
    
    var - you can declare the same variable
          multiple times in the same scope
    
    let - doesnot allow you to declare the same
          variable twice in a scope (throws an error)
        - if we want to access a variable in
          a particular scope, declare it using LET
          
   const - all features of LET , but Read-only
            cannot be reassigned 
          
    "use strict"; -> 
    - js is evaluated in strict mode
    - can be used to catch common coding mistakes
    - throws more errors , makes code robust, readable and accurate
    
*/

/*  
 we can however mutate(change) an array, with const keyword
 by using index.
 eg : const arr = [1, 2, 3]
      arr[0] = 100
      arr[1] = 200
      arr[2] = 300
*/



/*
Prevent Object mutation

const MATH_CONSTANTS = {
    PI : 3.14;
}
Object.freeze(MATH_CONSTANTS); //prevents PI from changing
MATH_CONSTANTS.PI = 2.4;        // results in an error NOW

hence a const declaration alone doesnt protect 
your object from mutation.
*/

/* Writing anonymous functions

// the old way
const magic = function(){ 
    return new Date(); 
}

// using ARROW function
const magic = () => new Date();


// ARROW functions with parameters;

var full_array = (arr1, arr2) => { arr1.concat(arr2); }


// template literal : - using backtick (`) 

*/

const person = {
    name: "prasad",
    age: 21
};

const greeting = `Hello, I am ${person.name} 
                        and I am ${person.age} years old.`

console.log(greeting)